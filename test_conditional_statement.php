<?php
$a=100;
//if..
      if($a>10)
             {
                echo "The value of a:"."  ".("$a");
              }
   
?>
<?php
$a=100;
//if..else
      if($a>10)
             {
                echo $a;
              }
      else{
                echo "a is not grater than 10";
             }
                echo "<br>";
                ?>
<?php
//if...elseif
$a=10;
$b=20;
if($a>100){
    echo"yes";
    
}
elseif ($b>=100) {
echo "b is grater then 100";
}
else{
    echo "No";
}
?>
<?php
//for switch statement
$a=2;
switch ($a){
    case 0:
        echo "0";
        break;
    case 1:
        echo "1";
        break;
    case 2:
        echo "2";
        break;
        default :
}
?>
<?php
//for while loop
$a=1;
while($a<=5){
    echo $a++;
    echo "<br>";
    
}
?>
<?php
//for do..while loop
$a=1;
do{
    echo $a;
    echo "<br>";
    
}
while ($a<=0)
?>
<?php
//for loop
for($a=1;$a<=10;$a++){
    echo $a;
    echo "<br>";
}
?>
<?php
//foreach loop
$arr=array("mango","banana");
foreach ($arr as $key => $value) {
    echo "$key <br>";
    echo "$value<br>";
    
    
}
?>
<?php
//for continue
$a=1;
while($a<=5){
    echo ++$a;
    echo "<br>";
    continue;
    if($a==6){
        echo"yes";
    }
    
}
?>
<?php
//for indexed array
$a=array("A","B","C");
echo "The value of array"."  "."=>".$a[0].", ".$a[1].", ".$a[2];
?>
<?php
//for indexed array loop
$a=array("A","B","C");
$my_choise=count($a);
for($x=0;$x<$my_choise;$x++){
    echo "$a[$x]<br>";
}
?>
<?php
//for Associative array
$a=array("A"=>"1","B"=>"2","C"=>"3");
foreach ($a as $name=>$value){
    echo "Name"." ".$name." & "."Value"." ".$value."<br>";
}
?>